//
//  AppDelegate.h
//  EcoSavannah3
//
//  Created by Shawn Cornelius on 7/16/15.
//  Copyright (c) 2015 Corn Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ECSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

