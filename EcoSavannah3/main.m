//
//  main.m
//  EcoSavannah3
//
//  Created by Shawn Cornelius on 7/16/15.
//  Copyright (c) 2015 Corn Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ECSAppDelegate class]));
    }
}
