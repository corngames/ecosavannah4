//
//  ECSPlant.h
//  EcoSavannah3
//
//  Created by Shawn Cornelius on 7/17/15.
//  Copyright (c) 2015 Corn Games. All rights reserved.
//


@interface ECSPlant : SKSpriteNode

- (id)initAtPosition:(CGPoint)position;
- (void)wasEaten:(float)amount;

@end
