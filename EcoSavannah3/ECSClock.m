//
//  ECSClock.m
//  EcoSavannah3
//
//  Created by Shawn Cornelius on 7/18/15.
//  Copyright (c) 2015 Corn Games. All rights reserved.
//

#import "ECSClock.h"

@interface ECSClock ()
@end

@implementation ECSClock

- (bool)isNightime {
    
    return FALSE;
};


- (bool)isDaytime {
    
    return TRUE;
    
};

@end
