//
//  ECSPlant.m
//  EcoSavannah3
//
//  Created by Shawn Cornelius on 7/17/15.
//  Copyright (c) 2015 Corn Games. All rights reserved.
//

#import "ECSPlant.h"
#import "ECSGameScene.h"


@interface ECSPlant ()
@end

@implementation ECSPlant


#pragma mark - Initialization
- (id)initAtPosition:(CGPoint)position {
    
    self = [ECSPlant spriteNodeWithImageNamed:@"Grass"];
    
    self.xScale = 0.2;
    self.yScale = 0.2;
    self.zPosition = 1.0;
    self.position = position;
    
    return self;
    
}

#pragma mark - Managing Available Food Level
- (void)wasEaten:(float)amount {
    
    // decrement available folliage
    
}

- (void)grow:(float)amount {
    
    // increment available folliage
    
}



@end


