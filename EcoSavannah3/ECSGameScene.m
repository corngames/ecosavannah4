//
//  GameScene.m
//  EcoSavannah3
//
//  Created by Shawn Cornelius on 7/16/15.
//  Copyright (c) 2015 Corn Games. All rights reserved.
//

#import "ECSGameScene.h"
#import "ECSUtilities.h"
#import "ECSGazelle.h"
#import "ECSPlant.h"
#import "ECSWater.h"

@interface ECSGameScene ()
//@property (nonatomic) APADataMapRef backgroundMap;
//@property (nonatomic) APATreeMapRef plantMap;
@property (nonatomic) int UserSelection;
@property (nonatomic, readwrite) NSMutableArray *plants;
@property (nonatomic) NSMutableArray *gazelles;
@property (nonatomic, readwrite) NSMutableArray *waters;
@property (nonatomic) NSTimeInterval lastUpdateTimeInterval; // the previous update: loop time interval

@end

@implementation ECSGameScene

#pragma mark - Data Maps
-(void)createDataMap:(NSString*)mapName {

    /*
     CGImageRef inImage = APAGetCGImageNamed(mapName);
     // Create the bitmap context.
     CGContextRef cgctx = APACreateARGBBitmapContext(inImage);
     
     if (cgctx == NULL) {    // error creating context
     return NULL;
     }
     
     // Get image width, height. We'll use the entire image.
     size_t w = CGImageGetWidth(inImage);
     size_t h = CGImageGetHeight(inImage);
     CGRect rect = {{0,0},{w,h}};
     
     // Draw the image to the bitmap context. Once we draw, the memory
     // allocated for the context for rendering will then contain the
     // raw image data in the specified color space.
     CGContextDrawImage(cgctx, rect, inImage);
     
     // Now we can get a pointer to the image data associated with the bitmap context.
     void *data = CGBitmapContextGetData(cgctx);
     
     // When finished, release the context.
     CGContextRelease(cgctx);
     
     return data;
     */
    
}

#pragma mark - Initialization and Deallocation
- (id)initWithSize:(CGSize)size {

    self = [super initWithSize:size];

    if (self) {
        
        // Build background and plant maps from image files.
//        _backgroundMap = [self createDataMap:@"map_level.png"];
//        _plantMap = [createDataMap:@"map_trees.png")];
        

//        [self buildWorld];
        
    }

    return self;
}

- (void)dealloc {

/*
        free(_backgroundMap);
        _backgroundMap = NULL;
*/
    
}


-(void)didMoveToView:(SKView *)view {

    /* Setup your scene here */
    _gazelles = [[NSMutableArray alloc] init];
    _plants = [[NSMutableArray alloc] init];
    _waters = [[NSMutableArray alloc] init];

    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    // Everytime the user touches the screen, instantiate either a plant, a gazelle, or a water element
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        if (_UserSelection == GAZELLE_MALE) {
            ECSGazelle *gazelle = [[ECSGazelle alloc] initAtPosition:location withGender:MALE];
            [self addChild:gazelle];
            [(NSMutableArray *)_gazelles addObject:gazelle];            
        }
        if (_UserSelection == GAZELLE_FEMALE) {
            ECSGazelle *gazelle = [[ECSGazelle alloc] initAtPosition:location withGender:FEMALE];
            [self addChild:gazelle];
            [(NSMutableArray *)_gazelles addObject:gazelle];
        }
        else if (_UserSelection == PLANT) {
            ECSPlant *plant = [[ECSPlant alloc] initAtPosition:location];
            [self addChild:plant];
            [(NSMutableArray *)_plants addObject:plant];
        }
        else if (_UserSelection == WATER) {
            ECSWater *water = [[ECSWater alloc] initAtPosition:location];
            [self addChild:water];
            [(NSMutableArray *)_waters addObject:water];
        }
    }

}

#pragma mark - Loop Update
- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)timeSinceLast {
    
    // Called before each frame is rendered
    
    for (ECSGazelle *gazelle in _gazelles) {
        [gazelle updateWithTimeSinceLastUpdate:timeSinceLast];
    }

}

- (void)update:(NSTimeInterval)currentTime {
    // Handle time delta.
    // If we drop below 60fps, we still want everything to move the same distance.
    CFTimeInterval timeSinceLast = currentTime - self.lastUpdateTimeInterval;
    self.lastUpdateTimeInterval = currentTime;
    
    [self updateWithTimeSinceLastUpdate:timeSinceLast];
    
}

#pragma mark - Manage Sprites
-(void)selectSprite:(int)userSelection {
    
    /* Called when a touch begins */
    _UserSelection = userSelection;
    
}

-(NSMutableArray *)getSprites:(int)spriteType {

    if (spriteType == GAZELLE) {
        return _gazelles;
    }
    else if (_UserSelection == PLANT) {
        return _plants;
    }
    else if (_UserSelection == WATER) {
        return _waters;
    }
    
    return _gazelles;
}

-(SKSpriteNode *)birthSprite:(SKSpriteNode *)spriteToBirth {
    
    // Add logic to create new sprites (for example, baby Gazelles)
    return spriteToBirth;
    
}

-(void)killSprite:(SKSpriteNode *)spriteToKill {

    // Add logic to destroy new sprites (for example, Gazelles that die)


}


@end















