//
//  GameScene.h
//  EcoSavannah3
//

//  Copyright (c) 2015 Corn Games. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface ECSGameScene : SKScene

-(void)selectSprite:(int)userSelection;
-(SKSpriteNode *)birthSprite:(SKSpriteNode *)spriteToBirth;
-(void)killSprite:(SKSpriteNode *)spriteToKill;
-(NSMutableArray *)getSprites:(int)spriteType;

@end
