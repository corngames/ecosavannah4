//
//  GameViewController.m
//  EcoSavannah3
//
//  Created by Shawn Cornelius on 7/16/15.
//  Copyright (c) 2015 Corn Games. All rights reserved.
//

#import "ECSGameViewController.h"
#import "ECSUtilities.h"
#import "ECSGameScene.h"

// Uncomment this line to show debug info:
#define SHOW_DEBUG_INFO 1

// Link Interface Builder objects here
@interface ECSGameViewController ()
@property (nonatomic) IBOutlet UIImageView *gameLogo;
//@property (nonatomic) IBOutlet UIImageView *gameBackground;
//@property (nonatomic) IBOutlet SKView *skView;
@property (nonatomic) ECSGameScene *scene;
@end


@implementation SKScene (Unarchive)

#pragma mark - Unpack Scene
+ (instancetype)unarchiveFromFile:(NSString *)file {

    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end


@implementation ECSGameViewController

#pragma mark - View Lifecycle
- (void)viewWillAppear:(BOOL)animated {

    // Load the shared assets of the scene before we initialize and load it.
    // PERFORMANCE IMPROVEMENT - SAMPLE IN ADVENTURE EXAMPLE

    // Configure the view.
    SKView * skView = (SKView *)self.view;
    
    // Create and configure the scene.
/*    ECSGameScene *scene = [ECSGameScene unarchiveFromFile:@"ECSGameScene"];
    scene.scaleMode = SKSceneScaleModeAspectFill;
*/
    _scene = [ECSGameScene unarchiveFromFile:@"ECSGameScene"];
    _scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:_scene];
    
    // Hide the game logo.
    [self.gameLogo setHidden:YES];

    // Hide the background.
    //[self.gameBackground setHidden:YES];

}
- (IBAction)pressPlantButton:(id)sender {

    // Tell the Scene that the user selected Plant
    [_scene selectSprite:PLANT];
    
}

- (IBAction)pressGazelleMaleButton:(id)sender {

    // Tell the Scene that the user selected a Male Gazelle
    [_scene selectSprite:GAZELLE_MALE];

}

- (IBAction)pressGazelleFemaleButton:(id)sender {
    
    // Tell the Scene that the user selected a Female Gazelle
    [_scene selectSprite:GAZELLE_FEMALE];
    
}

- (IBAction)pressWaterButton:(id)sender {

    // Tell the Scene that the user selected Water
    [_scene selectSprite:WATER];

}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
