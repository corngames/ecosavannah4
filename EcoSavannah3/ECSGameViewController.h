//
//  GameViewController.h
//  EcoSavannah3
//

//  Copyright (c) 2015 Corn Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface ECSGameViewController : UIViewController

@end
