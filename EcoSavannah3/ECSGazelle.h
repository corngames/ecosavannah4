//
//  ECSGazelle.h
//  EcoSavannah3
//
//  Created by Shawn Cornelius on 7/17/15.
//  Copyright (c) 2015 Corn Games. All rights reserved.
//

// Used by the move: method to move a character in a given direction.
typedef enum : uint8_t {
    ECSMoveDirectionForward = 0,
    ECSMoveDirectionLeft,
    ECSMoveDirectionRight,
    ECSMoveDirectionBack,
} ECSMoveDirection;

// The different animation states of an animated character.
typedef enum : uint8_t {
    ECSAnimationStateIdle = 0,
    ECSAnimationStateWalk,
    ECSAnimationStateAttack,
    ECSAnimationStateGetHit,
    ECSAnimationStateDeath,
    kAnimationStateCount
} ECSAnimationState;

/* 
// Bitmask for the different entities with physics bodies.
typedef enum : uint8_t {
    APAColliderTypeHero             = 1,
    APAColliderTypeGoblinOrBoss     = 2,
    APAColliderTypeProjectile       = 4,
    APAColliderTypeWall             = 8,
    APAColliderTypeCave             = 16
} APAColliderType;

*/

#define GazelleMovementSpeed 100.0

/*
#define kRotationSpeed 0.06
#define kCharacterCollisionRadius   40
#define kProjectileCollisionRadius  15
#define kDefaultNumberOfWalkFrames 28
#define kDefaultNumberOfIdleFrames 28
*/


@interface ECSGazelle : SKSpriteNode

@property (nonatomic) CGFloat movementSpeed;

- (id)initAtPosition:(CGPoint)position withGender:(int)gender;
- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)interval;
- (void)moveTowards:(CGPoint)position withTimeInterval:(NSTimeInterval)timeInterval;

@end
