//
//  ECSWater.h
//  EcoSavannah3
//
//  Created by Shawn Cornelius on 7/17/15.
//  Copyright (c) 2015 Corn Games. All rights reserved.
//


@interface ECSWater : SKSpriteNode

- (id)initAtPosition:(CGPoint)position;
- (void)wasDrunk:(float)amount;

@end
