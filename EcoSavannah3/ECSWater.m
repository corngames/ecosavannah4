//
//  ECSWater.m
//  EcoSavannah3
//
//  Created by Shawn Cornelius on 7/17/15.
//  Copyright (c) 2015 Corn Games. All rights reserved.
//

#import "ECSWater.h"
#import "ECSGameScene.h"


@interface ECSWater ()
@end

@implementation ECSWater


#pragma mark - Initialization
- (id)initAtPosition:(CGPoint)position {
    
    self = [ECSWater spriteNodeWithImageNamed:@"Water"];
    
    self.xScale = 0.2;
    self.yScale = 0.2;
    self.zPosition = 1.0;
    self.position = position;
    
    return self;
    
}

#pragma mark - Managing Water Level
- (void)wasDrunk:(float)amount {
    
     // decrement available water
    
}

- (void)refill:(float)amount {
    
    // increment available water
    
}


@end


