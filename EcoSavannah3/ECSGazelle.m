//
//  ECSGazelle.m
//  EcoSavannah3
//
//  Created by Shawn Cornelius on 7/17/15.
//  Copyright (c) 2015 Corn Games. All rights reserved.
//

#import "ECSGazelle.h"
#import "ECSGameScene.h"
#import "ECSUtilities.h"
#import "ECSWater.h"
#import "ECSPlant.h"

@interface ECSGazelle ()
@property (nonatomic) ECSAnimationState requestedAnimation;
@property (nonatomic) CGPoint currentDirection;
@property (nonatomic) int gender;


@end

@implementation ECSGazelle


#pragma mark - Initialization
- (id)initAtPosition:(CGPoint)position withGender:(int)gender {

    

    self = [ECSGazelle spriteNodeWithImageNamed:@"GazelleStanding"];
    
    self.xScale = 0.2;
    self.yScale = 0.2;
    self.zPosition = 1.0;
    self.position = position;
    self.movementSpeed = GazelleMovementSpeed * 0.35f;
    self.name = @"Gazelle";
    self.gender = gender;
    
    _currentDirection.x = 0;
    _currentDirection.y = 0;
    
    return self;

}


#pragma mark - Movement Functions
- (void)moveTowards:(CGPoint)position withTimeInterval:(NSTimeInterval)timeInterval {
    CGPoint curPosition = self.position;
    CGFloat dx = position.x - curPosition.x;
    CGFloat dy = position.y - curPosition.y;
    CGFloat dt = self.movementSpeed * timeInterval;
    
    CGFloat ang = ECS_POLAR_ADJUST(ECSRadiansBetweenPoints(position, curPosition));
    self.zRotation = ang;
    
    CGFloat distRemaining = hypotf(dx, dy);
    if (distRemaining < dt) {
        self.position = position;
    } else {
        self.position = CGPointMake(curPosition.x - sinf(ang)*dt,
                                    curPosition.y + cosf(ang)*dt);
    }
    
    self.requestedAnimation = ECSAnimationStateWalk;
}


- (void)moveInDirection:(CGPoint)direction withTimeInterval:(NSTimeInterval)timeInterval {
    CGPoint curPosition = self.position;
    CGFloat movementSpeed = self.movementSpeed;
    CGFloat dx = movementSpeed * direction.x;
    CGFloat dy = movementSpeed * direction.y;
    CGFloat dt = movementSpeed * timeInterval;
    
    CGPoint targetPosition = CGPointMake(curPosition.x + dx, curPosition.y + dy);
    
    CGFloat ang = ECS_POLAR_ADJUST(ECSRadiansBetweenPoints(targetPosition, curPosition));
    self.zRotation = ang;
    
    CGFloat distRemaining = hypotf(dx, dy);
    if (distRemaining < dt) {
        self.position = targetPosition;
    } else {
        self.position = CGPointMake(curPosition.x - sinf(ang)*dt,
                                    curPosition.y + cosf(ang)*dt);
    }

}


#pragma mark - Actions
- (void)wander:(CFTimeInterval)interval {
    
    CGPoint targetDirection = _currentDirection;

    // change direction every once and a while
    if ((arc4random() / (float)(0xffffffffu)) > .992) {

        targetDirection.x = ((arc4random() / (float)(0xffffffffu)) - (arc4random() / (float)(0xffffffffu)));
        targetDirection.y = ((arc4random() / (float)(0xffffffffu)) - (arc4random() / (float)(0xffffffffu)));
        
        _currentDirection = targetDirection;
        
    }
    
    [self moveInDirection:targetDirection withTimeInterval:interval];

}






#pragma mark - Loop Update
- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)interval {
    
    // Called before each frame is rendered
    
    // Add AI logic

    // Example - makes the gazelle wander
    [self wander:interval];
    
}

@end
 

